# translation of ports.po to தமிழ்
# ஆமாச்சு <amachu@ubuntu.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ports\n"
"PO-Revision-Date: 2007-11-02 22:27+0530\n"
"Last-Translator: ஆமாச்சு <amachu@ubuntu.com>\n"
"Language-Team: தமிழ் <https://lists.ubuntu.com/mailman/listinfo/ubuntu-l10n-"
"tam>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "ஆல்பாஃவுக்கான டெபியன்"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "பிஏ-ஆர்ஐஎஸ்சி க்கான டெபியன்"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "ஹர்ட் வட்டுக்கள்"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "ஐஏ-64 க்கான டெபியன்"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "தொடர்பு"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "மையப்பணிச்சாதனம்"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "சான்றுகள்"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "உருவாக்கம்"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "ஆவணமாக்கம்"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "நிறுவல்"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr ""

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "இணைப்புகள்"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "செய்திகள்"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "பெயர்த்தல்"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "துறைகள்"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "பிரச்சனைகள்"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "மென்பொருள் விளக்கி"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "நிலைமை"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "விநியோகம்"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "அமைப்புகள்"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "ஐ386 க்கான குனு/நெட் பிஎஸ்டி க்கான டெபியன்"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "ஆல்பாஃ க்கான குனு/நெட் பிஎஸ்டி க்கான டெபியன்"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "ஏன்"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "மக்கள்"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "பவர் கணினிக்கான டெபியன்"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "ஸ்பார்க்குக்கான டெபியன்"

#~ msgid "Debian for AMD64"
#~ msgstr "ஏஎம்டி64 க்கான டெபியன்"

#~ msgid "Debian for ARM"
#~ msgstr "ஏஆர்எம் க்கான டெபியன்"

#~ msgid "Debian for Beowulf"
#~ msgstr "பியோவுல்ப் க்கான டெபியன்"

#~ msgid "Main"
#~ msgstr "பிரதான"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "குனு/ப்ஃரீபிஎஸ்டி க்கான டெபியன்"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "மோடரோலா 680x0 க்கான"

#~ msgid "Debian for MIPS"
#~ msgstr "எம்ஐபிஎஸ் க்கான டெபியன்"

#~ msgid "Debian for S/390"
#~ msgstr "எஸ்/390 க்கான டெபியன்"

#~ msgid "Debian for Sparc64"
#~ msgstr "ஸ்பார்க்64 க்கான டெபியன்"

#~ msgid "Debian for Laptops"
#~ msgstr "மடிக்கணினிகளுக்கான டெபியன்"
