#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6"
<define-tag pagetitle>Clausura de DebConf21 online</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news

<p>
El sábado, 28 de agosto de 2021 se ha clausurado la conferencia anual de
desarrolladores y contribuidores de Debian.
</p>

<p>
DebConf21 ha sido la segunda Debconf que se ha celebrado en línea, debido a la pandemia
de enfermedad por el coronavirus (COVID-19). 
</p>

<p>
Todas las sesiones se han retransmitido y han contado con diferentes formas de participación:
a través de mensajería IRC, de documentos de texto colaborativos
y de salas de reuniones virtuales.
</p>

<p>
Con 740 participantes inscritos de 15 países y un
total de más de 70 charlas, debates,
reuniones informales (BoF, por sus siglas en inglés: «Birds of a Feather») y otras actividades,
<a href="https://debconf21.debconf.org">DebConf21</a> ha sido un gran éxito.
</p>

<p>
La configuración preparada para eventos en línea previos, que incluía Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad y una interfaz
web para voctomix, se ha mejorado y utilizado satisfactoriamente para DebConf21.
Todos los componentes de la infraestructura de vídeo son software libre y se configuran a través del
repositorio <a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a> público del Equipo de vídeo.
</p>

<p>
La <a href="https://debconf21.debconf.org/schedule/">programación</a> de la DebConf21 incluía
una amplia variedad de eventos, agrupados en varios canales:
</p>
<ul>
<li>Introducción al software libre &amp; Debian.</li>
<li>Empaquetado, normativa e infraestructura de Debian.</li>
<li>Administración, automatización y orquestación de sistemas.</li>
<li>Nube y contenedores.</li>
<li>Seguridad.</li>
<li>Comunidad, diversidad, compromiso local y contexto social.</li>
<li>Internacionalización, adaptación local («localización») y accesibilidad.</li>
<li>Embebido &amp; Núcleo.</li>
<li>Mezclas de Debian y distribuciones derivadas de Debian.</li>
<li>Debian en las artes &amp; Ciencia.</li>
<li>Y otros.</li>
</ul>
<p>
Las charlas se han retransmitido utilizando dos salas virtuales y
se han desarrollado en distintos idiomas: télugu, portugués, malabar, canarés,
hindi, maratí e inglés, posibilitando la asistencia y participación de una audiencia más diversa.
</p>

<p>
Entre charla y charla, la retransmisión de vídeo ha estado mostrando el habitual bucle de patrocinadores, pero también
algunos clips adicionales con fotos de DebConfs anteriores, hechos divertidos sobre Debian
y breves vídeos de reconocimiento enviados por los asistentes para comunicarse con sus amigos de Debian.
</p>

<p>El equipo de publicidad de Debian realizó la habitual «cobertura en directo» para animar a la participación
con micronoticias que anunciaban los eventos. El equipo de la DebConf también proporcionó varias
<a href="https://debconf21.debconf.org/schedule/mobile/">opciones para seguir la programación
desde el móvil</a>.
</p>

<p>
Para quienes no pudieron participar, la mayoría de las charlas y sesiones ya están
disponibles en el
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">archivo web de encuentros Debian</a>,
y el resto lo estará en los próximos días.
</p>

<p>
El sitio web <a href="https://debconf21.debconf.org/">DebConf21</a>
permanecerá activo como archivo y continuará ofreciendo
enlaces a las presentaciones y vídeos de charlas y eventos.
</p>

<p>
La celebración de <a href="https://wiki.debian.org/DebConf/22">DebConf22</a> está prevista para
julio de 2022 en Prizren, Kosovo.
</p>

<p>
La DebConf está comprometida con el establecimiento de un ambiente seguro y acogedor para todos y todas las participantes.
Durante la conferencia han estado disponibles varios equipos (Recepción, Equipo de bienvenida y Equipo
de comunidad) para ayudar a que quienes han participado hayan vivido la mejor experiencia
durante la conferencia y para ayudar a encontrar soluciones a cualquier problema que haya podido surgir.
Consulte la <a href="https://debconf21.debconf.org/about/coc/">página sobre el código de conducta en el sitio web de la DebConf21</a>
para más detalles sobre esta cuestión.
</p>

<p>
Debian agradece el compromiso de numerosos <a href="https://debconf21.debconf.org/sponsors/">patrocinadores</a>
por su apoyo a la DebConf21, especialmente el de nuestros patrocinadores platino:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
y <a href="https://google.com/">Google</a>.
</p>

<h2>Acerca de Debian</h2>
<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>

<h2>Acerca de DebConf</h2>

<p>
DebConf es la conferencia de desarrolladores del proyecto Debian. Además de un
amplio programa de charlas técnicas, sociales y sobre reglamentación, DebConf proporciona una
oportunidad para que desarrolladores, contribuidores y otras personas interesadas se
conozcan en persona y colaboren más estrechamente. Se ha celebrado
anualmente desde 2000 en lugares tan diversos como Escocia, Argentina y
Bosnia-Herzegovina. Hay más información sobre DebConf disponible en
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Acerca de Lenovo</h2>
<p>
Como líder tecnológico global en la fabricación de un amplio catálogo de productos conectados
incluyendo teléfonos inteligentes, tabletas, PC y estaciones de trabajo, dispositivos de realidad aumentada y de realidad virtual,
soluciones de hogar, oficina y centros de datos inteligentes, <a href="https://www.lenovo.com">Lenovo</a>
entiende lo críticos que son los sistemas y plataformas abiertos para un mundo conectado.
</p>

<h2>Acerca de Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> es la mayor empresa de alojamiento web de Suiza.
También ofrece servicios de respaldo y de almacenamiento, soluciones para organizadores de eventos y
servicios bajo demanda de retransmisión en directo («live-streaming») y vídeo.
Es propietaria de sus centros de proceso de datos y de todos los elementos críticos
para la prestación de los servicios y productos que suministra
(tanto software como hardware). 
</p>

<h2>Acerca de Roche</h2>
<p>
<a href="https://code4life.roche.com/">Roche</a> es una compañía de investigación y una gran proveedora farmacéutica internacional
dedicada a la asistencia médica personalizada. Más de 100&nbsp;000 empleados en todo el mundo
trabajan para resolver algunos de los mayores retos de la humanidad a través de
la ciencia y la tecnología. Roche participa activamente en proyectos de investigación
con financiación pública en colaboración con otros socios industriales y académicos
y ha dado soporte a la DebConf desde 2017.
</p>

<h2>Acerca de Amazon Web Services (AWS)</h2>
<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> es una de las plataformas
de nube más completas y más ampliamente adoptadas del mundo,
que ofrece más de 175 servicios con funcionalidades completas desde centros de datos globalizados
(en 77 zonas de disponibilidad dentro de 24 regiones geográficas).
Entre los clientes de AWS se encuentran las startups de más rápido crecimiento, las empresas más grandes
y agencias gubernamentales destacadas.
</p>

<h2>Acerca de Google</h2>
<p>
<a href="https://google.com/">Google</a> es una de las mayores empresas tecnológicas del
mundo y proporciona un amplio rango de servicios y productos relacionados con Internet tales
como tecnologías de publicidad en línea, búsqueda, computación en la nube, software y hardware.
</p>
<p>
Google ha dado soporte a Debian patrocinando la DebConf más de
diez años y es también socia de Debian patrocinando partes
de la infraestructura de integración continua de <a href="https://salsa.debian.org">Salsa</a>
en la plataforma de la nube de Google («Google Cloud Platform»).
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de DebConf21 en
<a href="https://debconf21.debconf.org/">https://debconf21.debconf.org/</a>
o envíe un correo electrónico a &lt;press@debian.org&gt;.</p>
