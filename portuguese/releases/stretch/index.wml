#use wml::debian::template title="Informações de lançamento do Debian &ldquo;stretch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/stretch/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="727dca1e3303ed66b1af9f096da919caee43f43c"

<p>O Debian <current_release_stretch> foi
lançado <a href="$(HOME)/News/<current_release_newsurl_stretch/>"><current_release_date_stretch>.
<ifneq "9.0" "<current_release>"
  "O Debian 9.0 foi incialmente lançado em <:=spokendate('2017-06-17'):>."
/>
O lançamento incluiu muitas
mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2017/20170617">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 9 foi substituído pelo
<a href="../buster/">Debian 10 (<q>buster</q>)</a>.
Atualizações de segurança foram descontinuadas em <:=spokendate('2020-07-06'):>.
</strong></p>

<p><strong>O Stretch se beneficia do suporte de longo prazo
(LTS - Long Term Support) até o final de junho de 2022. O LTS é limitado a
i386, amd64, armel, armhf e arm64.
Para mais informações, por favor consulte a
<a href="https://wiki.debian.org/LTS">seção LTS na wiki do Debian</a>.
</strong> </p>


<p>Para obter e instalar o Debian, consulte
a página <a href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">guia de instalação</a>. Para atualizar a partir de
uma versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>


# Ative o seguinte, quando o período LTS começar.
<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>

<ul>
<:
foreach $arch (@archeslts) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Arquiteturas de computadores suportadas no lançamento inicial do stretch:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Ao contrário do que desejamos, pode haver alguns problemas existentes
na versão, mesmo que ela seja declarada <em>estável (stable)</em>.
Nós fizemos <a href="errata">uma lista dos principais problemas conhecidos</a>,
e você sempre pode <a href="reportingbugs">relatar outros problemas</a>.</p>

<p>Por último, mas não menos importante, nós temos uma lista de
<a href="credits">pessoas que recebem o crédito</a> por fazer este lançamento
acontecer.</p>

<if-stable-release release="wheezy">
<p>Nenhuma informação disponível ainda.</p>
</if-stable-release>

<if-stable-release release="jessie">

<p>O codinome para a próxima versão principal do Debian após o <a
href="../jessie/">jessie</a> é <q>stretch</q>.</p>

<p>Esta versão começou como uma cópia da jessie, e está atualmente em um
estado chamado <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">
teste (testing)</a></q>. Isso significa que as coisas não deveriam quebrar
de maneira tão ruim quanto nas versões <q>instável (unstable)</q> ou
<q>experimental</q>, porque os pacotes só são autorizados a entrar nesta
versão depois que um certo período de tempo passou, e quando eles
não têm nenhum bug crítico ao lançamento relatado contra os mesmos.</p>

<p>Por favor, note que as atualizações de segurança para a versão
<q>teste (testing)</q> ainda <strong>não</strong> são gerenciadas pelo time de
segurança. Por isso, a <q>teste (testing)</q> <strong>não</strong> recebe
atualizações de segurança em tempo hábil.
# Para mais informações, por favor, veja o
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">anúncio</a>
# da equipe de segurança da testing.
Aconselhamos você a mudar suas entradas do sources.list de testing para
jessie por enquanto, se você precisa do suporte de segurança. Veja também
o texto na <a href="$(HOME)/security/faq#testing">FAQ da equipe de segurança</a>
para a versão <q>teste (testing)</q>.</p>

<p>Pode haver um <a href="releasenotes">rascunho das notas de lançamento disponível</a>.
Por favor, também <a href="https://bugs.debian.org/release-notes">
verifique as adições propostas para as notas de lançamento</a>.</p>

<p>Para imagens de instalação e documentação sobre como instalar a
<q>teste (testing)</q>, veja <a href="$(HOME)/devel/debian-installer/">a página
do instalador do Debian</a>.</p>

<p>Para descobrir mais sobre como a versão <q>teste (testing)</q> funciona,
verifique <a href="$(HOME)/devel/testing">as informações dos(as) desenvolvedores(as)
sobre ela</a>.</p>

<p>As pessoas frequentemente perguntam se há um único <q>medidor de progresso</q>
para o lançamento. Infelizmente não há, mas nós podemos nos referir a vários
locais que descrevem as coisas que precisam ser resolvidas para que o lançamento
aconteça:</p>

<ul>
  <li><a href="https://release.debian.org/">Página genérica de estado do lançamento</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Bugs críticos ao lançamento</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bugs no sistema básico</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bugs nos pacotes standard e task</a></li>
</ul>

<p>Além disso, relatórios de estado geral são enviados pelos(as) gerentes
de lançamento para a <a href="https://lists.debian.org/debian-devel-announce/">\
lista de discussão debian-devel-announce</a>.</p>

</if-stable-release>
