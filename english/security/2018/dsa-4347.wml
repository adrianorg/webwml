<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the implementation of the
Perl programming language. The Common Vulnerabilities and Exposures
project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

    <p>Jayakrishna Menon and Christophe Hauser discovered an integer
    overflow vulnerability in Perl_my_setenv leading to a heap-based
    buffer overflow with attacker-controlled input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

    <p>Eiichi Tsukata discovered that a crafted regular expression could
    cause a heap-based buffer overflow write during compilation,
    potentially allowing arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

    <p>Eiichi Tsukata discovered that a crafted regular expression could
    cause a heap-based buffer overflow read during compilation which
    leads to information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

    <p>Jakub Wilk discovered that a specially crafted regular expression
    could lead to a heap-based buffer overflow.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 5.24.1-3+deb9u5.</p>

<p>We recommend that you upgrade your perl packages.</p>

<p>For the detailed security status of perl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/perl">https://security-tracker.debian.org/tracker/perl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
# $Id: $
