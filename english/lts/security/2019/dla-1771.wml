<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14625">CVE-2018-14625</a>

    <p>A use-after-free bug was found in the vhost driver for the Virtual
    Socket protocol.  If this driver is used to communicate with a
    malicious virtual machine guest, the guest could read sensitive
    information from the host kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16884">CVE-2018-16884</a>

    <p>A flaw was found in the NFS 4.1 client implementation. Mounting
    NFS shares in multiple network namespaces at the same time could
    lead to a user-after-free. Local users might be able to use this
    for denial of service (memory corruption or crash) or possibly
    for privilege escalation.</p>

    <p>This can be mitigated by disabling unprivileged users from
    creating user namespaces, which is the default in Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19824">CVE-2018-19824</a>

    <p>Hui Peng and Mathias Payer discovered a use-after-free bug in the
    USB audio driver. A physically present attacker able to attach a
    specially designed USB device could use this for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19985">CVE-2018-19985</a>

    <p>Hui Peng and Mathias Payer discovered a missing bounds check in the
    hso USB serial driver. A physically present user able to attach a
    specially designed USB device could use this to read sensitive
    information from the kernel or to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20169">CVE-2018-20169</a>

    <p>Hui Peng and Mathias Payer discovered missing bounds checks in the
    USB core. A physically present attacker able to attach a specially
    designed USB device could use this to cause a denial of service
    (crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000026">CVE-2018-1000026</a>

    <p>It was discovered that Linux could forward aggregated network
    packets with a segmentation size too large for the output device.
    In the specific case of Broadcom NetXtremeII 10Gb adapters, this
    would result in a denial of service (firmware crash).  This update
    adds a mitigation to the bnx2x driver for this hardware.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3459">CVE-2019-3459</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-3460">CVE-2019-3460</a>

    <p>Shlomi Oberman, Yuli Shapiro and Karamba Security Ltd. research
    team discovered missing range checks in the Bluetooth L2CAP
    implementation.  If Bluetooth is enabled, a nearby attacker
    could use these to read sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3701">CVE-2019-3701</a>

    <p>Muyu Yu and Marcus Meissner reported that the CAN gateway
    implementation allowed the frame length to be modified, typically
    resulting in out-of-bounds memory-mapped I/O writes.  On a system
    with CAN devices present, a local user with CAP_NET_ADMIN
    capability in the initial net namespace could use this to cause a
    crash (oops) or other hardware-dependent impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3819">CVE-2019-3819</a>

    <p>A potential infinite loop was discovered in the HID debugfs
    interface exposed under /sys/kernel/debug/hid. A user with access
    to these files could use this for denial of service.</p>

    <p>This interface is only accessible to root by default, which fully
    mitigates the issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6974">CVE-2019-6974</a>

    <p>Jann Horn reported a use-after-free bug in KVM. A local user
    with access to /dev/kvm could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7221">CVE-2019-7221</a>

    <p>Jim Mattson and Felix Wilhelm reported a user-after-free bug in
    KVM's nested VMX implementation. On systems with Intel CPUs, a
    local user with access to /dev/kvm could use this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.</p>

    <p>Nested VMX is disabled by default, which fully mitigates the
    issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7222">CVE-2019-7222</a>

    <p>Felix Wilhelm reported an information leak in KVM for x86.
    A local user with access to /dev/kvm could use this to read
    sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8980">CVE-2019-8980</a>

    <p>A bug was discovered in the kernel_read_file() function used to
    load firmware files. In certain error conditions it could leak
    memory, which might lead to a denial of service. This is probbaly
    not exploitable in a Debian system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9213">CVE-2019-9213</a>

    <p>Jann Horn reported that privileged tasks could cause stack
    segments, including those in other processes, to grow downward to
    address 0. On systems lacking SMAP (x86) or PAN (ARM), this
    exacerbated other vulnerabilities: a null pointer dereference
    could be exploited for privilege escalation rather than only for
    denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.168-1~deb8u1.  This version also includes fixes for Debian
bugs #904385, #918103, and #922306; and other fixes included in upstream
stable updates.</p>

<p>We recommend that you upgrade your linux-4.9 and linux-latest-4.9
packages.  You will need to use <code>apt-get upgrade --with-new-pkgs</code>
or <code>apt upgrade</code> as the binary package names have changed.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1771.data"
# $Id: $
