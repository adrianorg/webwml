#use wml::debian::template title="Voorbeelden"
#use wml::debian::translation-check translation="d1af73e44d054ea9925b972ae2cb196c0365a2bb"

<h3>Een voorbeeld van hoe met een vertaling te beginnen</h3>

<p>We gebruiken Nederlands voor dit voorbeeld:

<pre>
   git pull
   cd webwml
   mkdir dutch
   cd dutch
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/dutch,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make* .wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.nl.po
</pre>

<p>Bewerk het bestand <tt>.wmlrc</tt> en maak de volgende aanpassingen:
<ul>
  <li>'-D CUR_LANG=English' aanpassen naar '-D CUR_LANG=Dutch'
  <li>'-D CUR_ISO_LANG=en' aanpassen naar '-D CUR_ISO_LANG=nl'
  <li>'-D CUR_LOCALE=en_US' aanpassen naar '-D CUR_LOCALE=nl_NL'
  <li>'-D CHARSET=utf-8' aanpassen naar wat passend is.<br>
      Nederlands gebruikt toevallig dezelfde tekencodering als Engels en dus is
      geen aanpassing nodig. Het is echter aannemelijk dat voor sommige nieuwe
      talen deze instelling aangepast zal moeten worden.
</ul>

<p>Bewerk Make.lang en verander 'LANGUAGE := en' in 'LANGUAGE := nl'.
Voor het geval u vertaalt naar een taal die een multi-byte tekenset gebruikt,
moet u in dat bestand mogelijk bepaalde andere variabelen aanpassen. Lees
voor verdere informatie ../Makefile.common en bekijk misschien ook andere
werkende voorbeelden (vertalingen zoals Chinees).

<p>Ga naar dutch/po en vertaal de tekstfragmenten uit de PO-bestanden. Dit zou
vrij eenvoudig moeten zijn.

<p>Zorg er steeds voor om het Makefile-bestand te kopiëren naar iedere map
die u vertaalt. Dit is noodzakelijk omdat het programma <code>make</code>
gebruikt wordt voor de omzetting van de .wml-bestanden naar HTML, en
<code>make</code> maakt gebruik van Makefile-bestanden.

<p>Wanneer u klaar bent met het toevoegen en bewerken van pagina's, moet u de commando's
<pre>
   git commit -m "Voeg hier uw vastleggingsbericht toe"
   git push
</pre>
uitvoeren in de map webwml. U kunt nu beginnen met het vertalen van de pagina's.

<h3>Een voorbeeld van het vertalen van een pagina</h3>

<p>De Nederlandse vertaling van het sociale contract zal voor dit voorbeeld gebruikt worden:

<pre>
   cd webwml
   ./copypage.pl -l dutch english/social_contract.wml
   cd dutch
</pre>

<p>Dit zal automatisch de kopregel translation-check toevoegen, die verwijst
naar de versie van het originele bestand dat gekopieerd werd. Dit maakt ook de
doelmap en het Makefile-bestand aan als die nog niet bestaan.</p>

<p>Bewerk social_contract.wml met een editor en vertaal de tekst. Tracht
geenszins eventuele links te vertalen of ze op een andere manier aan te
passen - indien u iets wenst te wijzigen, vraag dit dan aan op de mailinglijst
debian-www. Wanneer u klaar bent, type dan

<pre>
   git add social_contract.wml
   git commit -m "Translated social contract to dutch"
   git push
</pre>

<h3>Een voorbeeld van het toevoegen van een nieuwe map</h3>

<p>Dit voorbeeld toont hoe in de Nederlandse vertaling de map /intro toegevoegd wordt:

<pre>
   cd webwml/dutch
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m "added the intro dir to git"
   git push
</pre>

Zorg ervoor dat een nieuwe map het Makefile-bestand bevat en dat dit vastgelegd
werd in git. Anders zal het programma make een foutmelding geven aan iedere
andere persoon die het probeert uit te voeren.


 <h3>Een voorbeeld van een conflict</h3>

 <p>Dit voorbeeld toont een vastlegging die niet zal werken, omdat de kopie in
 het depot gewijzigd werd sinds uw laatste <kbd>git pull</kbd>.</p>

 <p>U heeft wat wijzigingen aangebracht in het bestand foo.wml. Dan zal:</p>

 <pre>
   git add foo.wml
   git commit -m "fixed a broken link"
   git push
 </pre>

 de volgende uitvoer geven:

 <pre>
To salsa.debian.org:webmaster-team/webwml.git
 ! [rejected]                master -> master (fetch first)
error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
 </pre>

 <p>of iets in die aard :)
       <br />
       <br />
    Dat betekent dat je wijzigingen <strong>niet</strong> naar het git-depot
    zijn gepusht vanwege conflicten.
       <br />
    U zult moeten onderzoeken wat er fout ging, de conflicten oplossen en
    opnieuw proberen te committen/pushen.</p>

