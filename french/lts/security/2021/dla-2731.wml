#use wml::debian::translation-check translation="04c1bcacfe6f155a7fdf70427fb320a3aca4dbbd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité affecte WordPress, un gestionnaire de contenu web,
dans ses versions entre 3.7 et 5.7. Cette mise à jour corrige les problèmes
de sécurité suivants :
injection d’objet dans PHPMailer (<a href="https://security-tracker.debian.org/tracker/CVE-2020-36326">CVE-2020-36326</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2018-19296">CVE-2018-19296</a>).</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 4.7.21+dfsg-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2731.data"
# $Id: $
