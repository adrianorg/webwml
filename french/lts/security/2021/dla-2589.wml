#use wml::debian::translation-check translation="62c2498f01aa954da087fb448911b8fd2dd28cec" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26519">CVE-2020-26519</a>

<p>Un défaut de dépassement de tampon de tas a été découvert dans MuPDF,
un afficheur léger de PDF. Il pourrait permettre l'exécution de code arbitraire
si des documents mal formés sont ouverts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3407">CVE-2021-3407</a>

<p>Une double libération d’objet lors d’une linéarisation a été découverte dans
MuPDF, un afficheur léger de PDF, qui pourrait conduire à une corruption de
mémoire et à d’autres conséquences possibles.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.9a+ds1-4+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mupdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mupdf, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mupdf">\
https://security-tracker.debian.org/tracker/mupdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2589.data"
# $Id: $
