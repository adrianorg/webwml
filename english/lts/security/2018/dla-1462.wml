<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerability was discovered in wpa_supplicant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14526">CVE-2018-14526</a>

    <p>An issue was discovered in rsn_supp/wpa.c in wpa_supplicant 2.0
    through 2.6. Under certain conditions, the integrity of EAPOL-Key
    messages is not checked, leading to a decryption oracle. An attacker
    within range of the Access Point and client can abuse the
    vulnerability to recover sensitive information.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.3-1+deb8u6.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS.">https://wiki.debian.org/LTS.</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1462.data"
# $Id: $
