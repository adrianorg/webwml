<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in uw-imap, the University of Washington
IMAP Toolkit, that might allow remote attackers to execute arbitrary OS
commands if the IMAP server name is untrusted input (e.g., entered by a
user of a web application) and if rsh has been replaced by a program
with different argument semantics.</p>

<p>This update disables access to IMAP mailboxes through running imapd over
rsh, and therefore ssh for users of the client application.  Code which
uses the library can still enable it with tcp_parameters() after making
sure that the IMAP server name is sanitized.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
8:2007f~dfsg-4+deb8u1.</p>

<p>We recommend that you upgrade your uw-imap packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1700.data"
# $Id: $
