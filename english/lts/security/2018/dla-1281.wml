<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Joonun Jang discovered that the advzip tool in advancecomp, a
collection of recompression utilities, was prone to a heap-based
buffer overflow. This might allow an attacker to cause a
denial-of-service (application crash) or other unspecified impact via
a crafted file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.15-1+deb7u1.</p>

<p>We recommend that you upgrade your advancecomp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1281.data"
# $Id: $
