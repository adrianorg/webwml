#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>ClamAV est un utilitaire d’anti-virus pour Unix dont les développeurs amont
ont publié la version 0.100.2. L’installation de cette nouvelle version est
nécessaire pour utiliser toutes les signatures actuelles de virus et pour
éviter les avertissements.</p>

<p>Cette version corrige aussi un problème de sécurité découvert après la
version 0.100.1 :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15378">CVE-2018-15378</a>

<p>Une vulnérabilité dans le décompresseur MEW de ClamAV peut permettre à des
attaquants non authentifiés de provoquer un déni de service à l'aide d'un
fichier EXE spécialement contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la
version 0.100.2+dfsg-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1553.data"
# $Id: $
