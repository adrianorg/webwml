<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Miguel Onoro reported that qbittorrent, a bittorrent client with a Qt5
GUI user interface, allows command injection via shell metacharacters in
the torrent name parameter or current tracker parameter, which could
result in remote command execution via a crafted name within an RSS feed
if qbittorrent is configured to run an external program on torrent
completion.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 3.3.7-3+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 4.1.5-1+deb10u1.</p>

<p>We recommend that you upgrade your qbittorrent packages.</p>

<p>For the detailed security status of qbittorrent please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qbittorrent">https://security-tracker.debian.org/tracker/qbittorrent</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4650.data"
# $Id: $
