#use wml::debian::template title="Porte MIPS"
#use wml::debian::translation-check translation="821d2af3a565be7b911813a3fb1a5543be4391e6"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="about">Visão geral</toc-add-entry>
<p>O porte MIPS é, de fato, dois portes, <em>debian-mips</em> e
<em>debian-mipsel</em>. Eles se diferenciam na <a
href="https://foldoc.org/endian">ordenação de bytes (endianness)</a>
dos binários. CPUs MIPS são capazes de executar com ambas as ordenações, mas
como isso normalmente não é alterável no software, nós precisamos ter
ambas as arquiteturas. Máquinas SGI executam no modo <a
href="https://foldoc.org/big-endian">byte mais significativo primeiro (big-endian)</a>
(debian-mips), enquanto as máquinas Loongson 3 executam no modo <a
href="https://foldoc.org/little-endian">byte menos significativo primeiro (little-endian)</a>
(debian-mipsel). Algumas placas, tais como a placa de avaliação BCM91250A da
Broadcom (vulgo SWARM) podem executar em ambos os modos, selecionáveis por uma
chave na placa. Algumas máquinas baseadas em Cavium Octeon podem trocar entre
ambos os modos no bootloader.</p>

<p>Dado que a maior parte das máquinas MIPS tem CPUs 64-bit, um porte
<em>debian-mips64el</em> está atualmente em desenvolvimento e poderá ser
lançado com o Debian GNU/Linux 9.</p>

<toc-add-entry name="status">Status atual</toc-add-entry>
<p>O Debian GNU/Linux <current_release_jessie> suporta as seguintes máquinas:</p>

<ul>

<li>SGI Indy com CPUs R4x00 e R5000, e Indigo2 com CPU R4400 (IP22).</li>

<li>SGI O2 com CPU R5000, R5200 e RM7000 (IP32).</li>

<li>Placa de avaliação Broadcom BCM91250A (SWARM) (big e little-endian).</li>

<li>Placas MIPS Malta (big e little-endian, 32 e 64-bit).</li>

<li>Máquinas Loongson 2e e 2f, incluindo o laptop Yeelong (little-endian).</li>

<li>Máquinas Loongson 3 (little-endian).</li>

<li>Cavium Octeon (big-endian).</li>

</ul>

<p>Além das máquinas acima, é possível usar o Debian em muito mais
máquinas, contanto que um kernel não Debian seja usado. Este é o caso,
por exemplo, da placa de desenvolvimento MIPS Creator Ci20.</p>

<toc-add-entry name="info">Informações gerais</toc-add-entry>

<p>Por favor, veja as <a href="$(HOME)/releases/stable/mips/release-notes/">\
notas de lançamento</a> e o <a href="$(HOME)/releases/stable/mips/">\
manual de instalação</a> para mais informações.</p>


<toc-add-entry name="availablehw">Hardware disponível para desenvolvedores(as) Debian</toc-add-entry>

<p>Duas máquinas estão disponíveis para desenvolvedores(as) Debian para trabalho
com o porte do MIPS: etler.debian.org (mipsel/mips64el) e minkus.debian.org
(mips). As máquinas têm ambientes de desenvolvimento chroot que você pode
acessar com <em>schroot</em>. Por favor, veja o
<a href = "https://db.debian.org/machines.cgi">banco de dados de máquinas</a>
para mais informações sobre essas máquinas.</p>


<toc-add-entry name="credits">Créditos</toc-add-entry>

<p>Esta é uma lista de pessoas que estão trabalhando no porte MIPS:</p>

#include "$(ENGLISHDIR)/ports/mips/people.inc"

<toc-add-entry name="contacts">Contatos</toc-add-entry>

<h3>Listas de discussão</h3>

<p>Existem algumas listas de discussão lidando com o Linux/MIPS e especialmente
o Debian no MIPS.</p>

<ul>

<li>debian-mips@lists.debian.org &mdash; Esta lista lida com Debian no MIPS.<br />
Inscreva-se via e-mail em <email debian-mips-request@lists.debian.org>.<br />
O arquivo está em <a href="https://lists.debian.org/debian-mips/">lists.debian.org</a>.</li>

<li>linux-mips@linux-mips.org &mdash; Esta lista é para discussões
sobre suportes ao kernel MIPS.<br />
Veja a página <a href = "https://www.linux-mips.org/wiki/Net_Resources#Mailing_lists">Linux/MIPS</a>
para informações de inscrição.</li>

</ul>

<h3>IRC</h3>

<p>Você pode nos encontrar no IRC em <em>irc.debian.org</em> no canal
<em>#debian-mips</em>.</p>


<toc-add-entry name="links">Links</toc-add-entry>

<dl>
  <dt>Desenvolvimento do kernel Linux/MIPS &mdash;  Muitas informações relativas a MIPS</dt>
    <dd><a href="https://www.linux-mips.org/">linux-mips.org</a></dd>
  <dt>Fornecedor de CPU</dt>
    <dd><a href="https://imgtec.com/mips">https://imgtec.com/mips</a></dd>
  <dt>Informações sobre hardware SGI</dt>
    <dd><a href="http://www.sgistuff.net/hardware/">http://www.sgistuff.net/hardware/</a></dd>
  <dt>Debian no SGI Indy</dt>
    <dd><a href="http://www.pvv.org/~pladsen/Indy/HOWTO.html">http://www.pvv.org/~pladsen/Indy/HOWTO.html</a></dd>
  <dt>Debian no SGI Indy</dt>
    <dd><a href="https://nathan.chantrell.net/linux/sgi-indy-and-debian-linux/">https://nathan.chantrell.net/linux/sgi-indy-and-debian-linux/</a></dd>
  <dt>Debian no SGI O2</dt>
    <dd><a href="https://cyrius.com/debian/o2/">http://www.cyrius.com/debian/o2</a></dd>
</dl>


<toc-add-entry name="thanks">Agradecimentos</toc-add-entry>

<p>As máquinas de porte e a maioria dos servidores de construção para arquiteturas
<em>mips</em> e <em>mipsel</em> são fornecidas por <a href="https://imgtec.com">
Imagination Technologies</a>.</p>


<toc-add-entry name="dedication">Dedicatória</toc-add-entry>

<p>Thiemo Seufer, que foi o portador líder do MIPS no Debian, faleceu em um
acidente de carro. Nós <a href =
"$(HOME)/News/2008/20081229">dedicamos o lançamento</a> da distribuição
Debian GNU/Linux <q>lenny</q> em sua homenagem.</p>
